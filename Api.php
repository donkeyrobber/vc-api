<?php

class Api{

	private $request_method;

	public function __construct($request_method){

		$this->request_method = $request_method;

	}

	public function requestCall($args){

		$errors = array();
		
		if($this->request_method !== "POST" ){

			throw new Exception("request-call must be called as a POST method");

		}

		$first_name = isset($args['first_name']) ? trim($args['first_name']) : null ;

		if( ! $first_name ){

			$errors['first_name'] = "Please enter a valid first name";

		}

	        $last_name = isset($args['last_name']) ? trim($args['last_name']) : null ;

                if( ! $last_name ){

                        $errors['last_name'] = 'Please enter a valid last name';
                
                }

            	$email = isset($args['email']) ?  filter_var($args['email'],FILTER_VALIDATE_EMAIL) : null ;

                if( ! $email ){

                        $errors['email'] = 'Please enter a valid email address';
                
                }

		$telephone =  isset($args['telephone']) ?  trim($args['telephone']) : null ;

                if(! $telephone ){
        
                        $errors['telephone'] = 'Please enter a valid telephone number';

                }	

		if( count( $errors ) > 0 ){

			throw new InvalidArgumentException(json_encode($errors));

		}
		
		$comment = isset($args['comment']) ? $args['comment'] : null ;

		try{
			$db = new PDO('mysql:dbname=venuecreation;host=localhost','vc','OfGc6nDcs');

		}catch(PDOException $e){

			throw new Exception("Can't connect to DB:" . $e->getMessage());

		}

		try{
			$sql = "SELECT id FROM contacts WHERE email = :email";

			$stmt = $db->prepare($sql);

			$stmt->bindParam(':email',$email);

			$stmt->execute();

			$contact = $stmt->fetch();			



		}catch(PDOException $e){

			throw new Exception("Can't execute query: ".$e->getMessage());

		}


		try{
			if(isset($contact['id'])){

				$sql = "UPDATE contacts SET 
					first_name = :first_name,
					last_name = :last_name,
					telephone = :telephone,
					WHERE
					email=:email";
			
				$contact_id = $contact['id'];

			}else{

	
				$sql = "INSERT INTO contacts (first_name,last_name,email,telephone)
					VALUES(:first_name,
					:last_name,
					:email,
					:telephone)";

			}

			$stmt = $db->prepare($sql);

			$stmt->bindParam(":first_name",$first_name);
	                $stmt->bindParam(":last_name",$last_name);
                	$stmt->bindParam(":telephone",$telephone);
        	        $stmt->bindParam(":email",$email);
			$stmt->execute();

		}catch(PDOException $e){

			throw new Exception("Can not execute query: " . $e->getMessage());

		}
		
		$contact_id = ( ! isset( $contact_id ) ) ?  $db->lastInsertId() : $contact_id ;
		try{
		
			$sql = "INSERT INTO comments (contact_id,comment)
			VALUES(:contact_id, 
			:comment)";

			$stmt = $db->prepare($sql);

			$stmt->bindParam(":contact_id",$contact_id,PDO::PARAM_INT);
        	        $stmt->bindParam(":comment",$comment,PDO::PARAM_STR);

			$stmt->execute();

		}catch(PDOException $e){

			throw new Exception("Can not execute query: ". $e->getMeesage());

		}
		
		$html="

		<html>
			<head>
				<title>Web Enquiry</title>
			</head>
			<body>
				<table border=\"0\">

					<tr><td>First Name</td><td>".$first_name."</td></tr>
					<tr><td>Last Name</td><td>".$last_name."</td></tr>
					<tr><td>Email</td><td>".$email."</td></tr>
					<tr><td>Telephone</td><td>".$telephone."</td></tr>
					<tr><td>Comment</td><td>".$comment."</td></tr>

				</table>
			</body>
		</html>";

		$headers = "From: web-enquiries@venue-creation.co.uk" . "\r\n" .
                                "Content-type: text/html; charset=iso-8859-1" . "\r\n" .    				"Reply-To: no-reply@venue-creation.co.uk" . "\r\n" .
    				"X-Mailer: PHP/" . phpversion();

		if( ! mail("info@venue-creation.co.uk", "Web Enquiry", $html, $headers) ){

			throw new Exception("Can not send email to venue-creation");

		}

		return "Thank you for your request";

	}

}
