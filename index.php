<?php
require "Api.php";

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Max-Age: 1000');

$request = parse_url($_SERVER['PHP_SELF']);

if( ! $request ){

	http_response('Service not available',
			500);

}

$action = explode('/',trim($request['path'], '/'));

if($action[0] === 'index.php' ){

        array_shift($action);

}

switch($action[0]){

	case "api":

		if( ! isset($_SERVER['REQUEST_METHOD'])){

		        http_response('Service not available',
					500);

		}

		$request_method = $_SERVER['REQUEST_METHOD'];

		$data = ($request_method=='POST') ? $_POST : $_GET;

		$api = new Api($request_method);
		
		$segments = explode('-',$action[1]);
		
		$method = array_shift($segments);

		if( count($segments) > 0 ){

			foreach($segments as $segment){

				$method .= ucfirst($segment);

			}

		}

		if( ! method_exists($api, $method)){

			http_response('Resouce ' . $action[1] . ' is not available',
					404);

		}

		try{
			$api_response = $api->$method($data);
		
		}catch(InvalidArgumentException $e){

			$errors = $e->getMessage();

			http_response($errors,
					400,
					false);

		}catch(Exception $e){

			http_response($e->getMessage(),
					400);

		}

		http_response($api_response,
				200);
				
	break;

}


function http_response($message, $status_code = 200, $encode = true ){

	header('Content-type:application/json', $status_code);

	if( $encode ){

		$message = json_encode($message);
	}
	echo $message;

	exit();

}
